<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ClocksSettings</name>
    <message>
        <location filename="../ClocksSettings.ui" line="17"/>
        <location filename="../ClocksSettings.ui" line="43"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="94"/>
        <source>Left Up</source>
        <translation>Слева вверху</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="101"/>
        <source>Right Up</source>
        <translation>Справо вверху</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="115"/>
        <source>Left Down</source>
        <translation>Слева внизу</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="122"/>
        <source>Right Down</source>
        <translation>Справа внизу</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="203"/>
        <location filename="../ClocksSettings.ui" line="224"/>
        <location filename="../ClocksSettings.ui" line="469"/>
        <source>hh:mm:ss</source>
        <translation>hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="210"/>
        <location filename="../ClocksSettings.cpp" line="131"/>
        <source>Enable Alarm</source>
        <translation>Вкл. будильник</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="231"/>
        <location filename="../ClocksSettings.cpp" line="186"/>
        <source>Enable Timer</source>
        <translation>Вкл. таймер</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="242"/>
        <source>Clocks style</source>
        <translation>Стиль часов</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="462"/>
        <source>Clocks Type</source>
        <translation>Тип часов</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="137"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="144"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="155"/>
        <location filename="../ClocksSettings.ui" line="162"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="425"/>
        <source>Font Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="302"/>
        <source>Transparent</source>
        <translation>Прознрачность</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="309"/>
        <source>Red</source>
        <translation>Красный</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="316"/>
        <source>Green</source>
        <translation>Зеленый</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="323"/>
        <source>Blue</source>
        <translation>Синий</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="267"/>
        <source>White</source>
        <translation>Белый</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="274"/>
        <source>Grey</source>
        <translation>Серый</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="281"/>
        <source>Black</source>
        <translation>Черный</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="182"/>
        <source>Display timer instead clocks</source>
        <translation>Показывать таймер вместо часов</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="400"/>
        <source>Use CSS</source>
        <translation>Использовать CSS</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="58"/>
        <source>Exports settings</source>
        <translation>Экспорт настроек</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="65"/>
        <location filename="../ClocksSettings.cpp" line="252"/>
        <source>Import Settings</source>
        <translation>Импорт настроек</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="484"/>
        <source>About Program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="499"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;Clocks On Windows&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;Developer: Dmitry Minko (Roker2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;Sponsors:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;• Akali&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;Clocks On Windows&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;Разработчик: Минько Дмитрий (Roker2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;Спонсоры:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:9pt;&quot;&gt;• Akali&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="522"/>
        <source>Sources</source>
        <translation>Исходники</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.ui" line="529"/>
        <source>Donation</source>
        <translation>Пожертвование</translation>
    </message>
    <message>
        <source>Clocks On Windows Settings</source>
        <translation type="vanished">Настройки Clocks On Settings</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.cpp" line="125"/>
        <source>Disable Alarm</source>
        <translation>Выкл. будильник</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.cpp" line="176"/>
        <source>Disable Timer</source>
        <translation>Выкл. таймер</translation>
    </message>
    <message>
        <location filename="../ClocksSettings.cpp" line="298"/>
        <source>Export Settings</source>
        <translation>Экспорт настроек</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <location filename="../mainwindow.cpp" line="23"/>
        <source>Clocks On Windows</source>
        <translation>Clocks On Windows</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="15"/>
        <source>Open settings</source>
        <translation>Открыть настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="16"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>QTimeAlarm</name>
    <message>
        <location filename="../QTimeAlarm.cpp" line="26"/>
        <source>Alarm</source>
        <translation>Будильник</translation>
    </message>
    <message>
        <location filename="../QTimeAlarm.cpp" line="26"/>
        <source>Current time is </source>
        <comment>Alarm: It is show current time</comment>
        <translation>Текущее время </translation>
    </message>
    <message>
        <location filename="../QTimeAlarm.cpp" line="37"/>
        <source>Timer</source>
        <translation>Таймер</translation>
    </message>
    <message>
        <location filename="../QTimeAlarm.cpp" line="37"/>
        <source>Timer over</source>
        <translation>Время вышло</translation>
    </message>
</context>
</TS>
