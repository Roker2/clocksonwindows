#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QApplication a(argc, argv);
    QTranslator LanguageTranslator;
    LanguageTranslator.load(":translations/languages/strings_" + QLocale::system().name());
    qApp->installTranslator(&LanguageTranslator);
    MainWindow w;
    w.setWindowFlag(Qt::WindowStaysOnTopHint);
    w.setWindowFlag(Qt::FramelessWindowHint);//SplashScreen FramelessWindowHint
    w.setAttribute(Qt::WA_TranslucentBackground);
    w.show();

    return a.exec();
}
