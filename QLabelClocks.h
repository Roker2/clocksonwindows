#ifndef QLABELCLOCKS_H
#define QLABELCLOCKS_H
#include <QLabel>
#include <QTime>
#include <QPushButton>
#include <QFontMetrics>
#include "ClocksSettings.h"

class QLabelClocks : public QLabel
{
    Q_OBJECT

public:
    QLabelClocks(QWidget *parent, ClocksSettings *settings_menu);
    virtual ~QLabelClocks() override;
    void set_settings_menu(ClocksSettings *temp);
    void init();
    void set_is_moving(bool is_moving);

signals:
    void clocksWidthChanged(int clocks_width);

public slots:
    void update_style();
    void set_red(int red);
    void set_green(int green);
    void set_blue(int blue);
    void set_alpha(int alpha);
    void font_changed();
    void set_font_size(int size);
    void set_clocks_type(const QString &type);
    void set_css_style(const QString &CSSStyle);
    void set_current_time();
    void set_time(QTime *time);

protected:
    virtual void timerEvent(QTimerEvent*) override;

private:
    int get_width_label();
    int get_height_label();
    void change_color();
    void set_size_and_position();
    void set_main_menu_geometry();
    int get_font_height();

private:
    int _red;
    int _green;
    int _blue;
    int _alpha;
    int x = 0, y = 0;
    int _font_size;
    int _plus_width;
    QString _clocks_type;
    bool _is_moving = false;
    ClocksSettings *_settings_menu = nullptr;
    QWidget *_main_menu = nullptr;
};

#endif // QLABELCLOCKS_H
