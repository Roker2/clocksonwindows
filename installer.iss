; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Clocks On Windows"
#define MyAppVersion "0.5.1.0"
#define MyAppPublisher "Dmitry Minko (Roker2)"
#define MyAppURL "https://sourceforge.net/projects/clocks-on-windows/files/"
#define MyAppExeName "ClocksOnWindows.exe"
#define PathToFiles "C:\Users\user\Desktop\�����"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{00A2B547-69A8-4255-B090-43FC00B7EAA3}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\{#MyAppName}
DisableProgramGroupPage=yes
; The [Icons] "quicklaunchicon" entry uses {userappdata} but its [Tasks] entry has a proper IsAdminInstallMode Check.
UsedUserAreasWarning=no
LicenseFile=E:\ClocksOnWindows\staging\LICENSE
; InfoAfterFile=Thank you for installing!
; Uncomment the following line to run in non administrative install mode (install for current user only.)
;PrivilegesRequired=lowest
OutputDir=C:\Users\user\Desktop\�����\installer
OutputBaseFilename=ClocksOnWindows
SetupIconFile=E:\ClocksOnWindows\staging\icons\baseline_extension_white_48dp.ico
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 6.1; Check: not IsAdminInstallMode

[Files]
Source: "{#PathToFiles}\ClocksOnWindows.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\libgcc_s_dw2-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\libstdc++-6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\libwinpthread-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\qt.conf"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\Qt5Multimedia.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\Qt5Network.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#PathToFiles}\plugins\*"; DestDir: "{app}\plugins"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#PathToFiles}\sounds\*"; DestDir: "{app}\sounds"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#PathToFiles}\themes\*"; DestDir: "{app}\themes"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

