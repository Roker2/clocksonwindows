#include "QLabelClocks.h"

QLabelClocks::QLabelClocks(QWidget *parent, ClocksSettings *settings_menu) :
    QLabel(parent)
    , _red(255)
    , _green(255)
    , _blue(255)
    , _alpha(80)
    , _font_size(36)
    ,_plus_width(-7)
    , _clocks_type("hh:mm:ss")
{
    setWindowFlag(Qt::WindowStaysOnTopHint);
    setWindowFlag(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);
    _main_menu = parent;
    _settings_menu = settings_menu;
    startTimer(100);
}

QLabelClocks::~QLabelClocks()
{
}

int QLabelClocks::get_width_label()
{
    QFontMetrics FontInfo(fontMetrics());
    int temp = 0;
    QString str = _settings_menu && _settings_menu->get_timer_state() ? _settings_menu->get_timer_value().toString(_clocks_type) : (QTime::currentTime().toString(_clocks_type));
    for (int i = 0; i < str.length(); i++)
    {
        temp += FontInfo.horizontalAdvance(str.at(i));
        /*temp += FontInfo.leftBearing(ClocksType.at(i));
        temp += FontInfo.rightBearing(ClocksType.at(i));*/
    }
    return temp;
}

int QLabelClocks::get_height_label()
{
    QFontMetrics FontInfo(fontMetrics());
    return FontInfo.height();
}

int QLabelClocks::get_font_height()
{
    QFont temp = this->font();
    temp.setPixelSize(_settings_menu->get_font_size_max());
    QFontMetrics FontInfo(temp);
    return FontInfo.height();
}

void QLabelClocks::timerEvent(QTimerEvent*)
{
    set_size_and_position();
    int clocks_width = get_width_label();
    setGeometry(0, 0, clocks_width, get_height_label());
    emit clocksWidthChanged(clocks_width);
}

void QLabelClocks::change_color()
{
    setStyleSheet("QLabel {color : rgba(" + QString::number(_red) + ", " + QString::number(_green) + ", " + QString::number(_blue) + ", " + QString::number(_alpha) + "); font-size:" + QString::number(_font_size) + "px}"); //"QLabel {color : rgba(0, 0, 255, value); font-size:36px}"
}

void QLabelClocks::set_size_and_position()
{
    if (_settings_menu)
    {
        if (_is_moving)
        {
            return;
        }
        if (x != _settings_menu->get_coordinate_x() || y != _settings_menu->get_coordinate_y())
        {
            x = _settings_menu->get_coordinate_x();
            y = _settings_menu->get_coordinate_y();
            _main_menu->move(x, y);
        }
    }
}

void QLabelClocks::set_main_menu_geometry()
{
    _main_menu->move(x, y);
    int clocks_width = get_width_label();
    _main_menu->setFixedSize(clocks_width, get_height_label());
    emit clocksWidthChanged(clocks_width);
}

void QLabelClocks::set_settings_menu(ClocksSettings *temp)
{
    _settings_menu = temp;
}

void QLabelClocks::set_current_time()
{
    setText(QTime::currentTime().toString(_clocks_type));
}

void QLabelClocks::set_time(QTime *time)
{
    setText(time->toString(_clocks_type));
}

void QLabelClocks::set_css_style(const QString &CSSStyle)
{
    setStyleSheet("QLabel {" + CSSStyle + "font-size:" + QString::number(_font_size) + "px}");
}

void QLabelClocks::update_style()
{
    if(_settings_menu->get_use_css() == Qt::CheckState())
    {
        change_color();
    }
    else
    {
        set_css_style(_settings_menu->get_clocks_css_style());
    }
}

void QLabelClocks::set_red(int red)
{
    _red = red;
    update_style();
}

void QLabelClocks::set_green(int green)
{
    _green = green;
    update_style();
}

void QLabelClocks::set_blue(int blue)
{
    _blue = blue;
    update_style();
}

void QLabelClocks::set_alpha(int alpha)
{
    _alpha = alpha;
    update_style();
}

void QLabelClocks::font_changed()
{
    set_main_menu_geometry();
}

void QLabelClocks::set_font_size(int size)
{
    if (_font_size != size)
    {
        _font_size = size;
        update_style();
        set_main_menu_geometry();
    }
}

void QLabelClocks::set_clocks_type(const QString &type)
{
    if (_clocks_type != type)
    {
        _clocks_type = type;
        set_main_menu_geometry();
    }
}

void QLabelClocks::init()
{
    set_font_size(_settings_menu->get_font_size());
    set_clocks_type(_settings_menu->get_type_clocks());
    update_style();
    set_main_menu_geometry();
}

void QLabelClocks::set_is_moving(bool is_moving)
{
    _is_moving = is_moving;
}
