#ifndef CLOCKS_H
#define CLOCKS_H

#include <QWidget>
#include <QString>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QDesktopServices>
#include "SaveRestoreSettings/QSaveSettings.h"
#include "SaveRestoreSettings/QRestoreSettings.h"

namespace Ui {
class ClocksSettings;
}

class ClocksSettings : public QWidget
{
    Q_OBJECT

    using QSaveSettingsPtr = std::unique_ptr<QSaveSettings>;
    using QRestoreSettingsPtr = std::unique_ptr<QRestoreSettings>;

public:
    explicit ClocksSettings(QWidget *parent = nullptr);
    ~ClocksSettings();
    int get_coordinate_x();
    int get_coordinate_y();
    int get_font_size();
    int get_font_size_max();
    QTime alarm_time_value();
    bool get_alarm_state();
    bool get_timer_state();
    QString get_type_clocks();
    QTime get_timer_value();
    Qt::CheckState get_timer_or_clocks();
    QString get_css_style();
    Qt::CheckState get_use_css();
    QString get_clocks_css_style();
    void set_coordinate_x(int value);
    void set_coordinate_y(int value);
    void restore_settings();

public slots:
    void disable_alarm();
    void disable_timer();
    void timer_minus_time(int milliseconds);
    void set_clocks_width(int clocks_width);

private:
    void enable_alarm();
    void enable_timer();
    void RestoreSettings_End();
    void SaveSettings();
    void SaveSettings_Start();

private slots:
    void on_Button_SetWhite_clicked();
    void on_Button_SetGrey_clicked();
    void on_Button_SetBlack_clicked();
    void on_fontComboBox_currentFontChanged(const QFont &f);
    void on_Button_RightUp_clicked();
    void on_Button_RightDown_clicked();
    void on_Button_LeftDown_clicked();
    void on_Button_LeftUp_clicked();
    void on_Button_Enable_Disable_Alarm_clicked();
    void on_Button_Enable_Disable_Timer_clicked();
    void on_Slider_Red_valueChanged(int value);
    void on_Slider_Green_valueChanged(int value);
    void on_Slider_Blue_valueChanged(int value);
    void on_Slider_Transparent_valueChanged(int value);
    void on_Button_ExportSettings_clicked();
    void on_Button_ImportSettings_clicked();
    void on_AboutProgram_OpenSources_clicked();
    void on_AboutProgram_Donation_clicked();
    void on_checkBox_UseCSS_stateChanged(int arg1);
    void on_textEdit_CSS_textChanged();
    void on_Slider_FontSize_valueChanged(int value);
    void on_Edit_ClocksType_textChanged(const QString &arg1);

private:
    Ui::ClocksSettings *ui;
    bool _alarm_state{ false };
    bool _timer_state{ false };
    int _red = 255;
    int _green = 255;
    int _blue = 255;
    int _alpha = 80;
    int _clocks_width;
    QString _css_style;
    const QString _theme_name = "StandartTheme";
    QSaveSettingsPtr _save_settings;
    QRestoreSettingsPtr _restore_settings;

signals:
    void fontChanged();
    void fontSizeChanged(int size);
    void clocksTypeChanged(const QString &type);
    void cssStyleChanged(const QString &CSSStyle);
    void enableTimer(int h, int m, int s, int ms);
    void disableTimer();
    void redChanged(int red);
    void greenChanged(int green);
    void blueChanged(int blue);
    void alphaChanged(int alpha);
};

#endif // CLOCKS_H
