#ifndef QTIMEALARM_H
#define QTIMEALARM_H

#include <QObject>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QDir>
#include <QUrl>
#include <QMessageBox>
#include "QLabelClocks.h"
#include "ClocksSettings.h"

class QTimeAlarm : public QObject
{
    Q_OBJECT

public:
    explicit QTimeAlarm(QObject *parent = nullptr);
    void set_settings_menu(ClocksSettings *settings_menu);

public slots:
    void enable_timer(int h, int m, int s, int ms);
    void disable_timer();

protected:
    virtual void timerEvent(QTimerEvent*);

signals:
    void setCurrentTime();
    void sendTime(QTime *time);
    void disableTimer();
    void disableAlarm();
    void timerMinusTime(int milliseconds);

private:
    void enable_music();
    void set_clocks();
    void show_message(QString header, QString message);

private slots:
    void disable_music();

private:
    ClocksSettings *_settings_menu;
    QMediaPlayer *_alarm_player;
    QMediaPlaylist *_alarm_music;
    QTime _timer;
    int _timer_event_period;
    bool _timer_is_enabled{ false };
    const QString _default_clocks_type;
    const QString _zero_time;
    const QString _path_to_sound;
};

#endif // QTIMEALARM_H
