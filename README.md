<div align="center">
	<h1>Clocks On Windows</h1>
	<img src="https://gitlab.com/Roker2/clocksonwindows/raw/staging/icons/baseline_schedule_white_48dp.png" width="76" align="center" />
	<br/> <br/>
</div>

| Feature                 | Specification       |
| :---------------------- | :------------------ |
| Language                | C++                 |
| IDE                     | Qt Creator          |
| Installer program       | Inno Setup Compiler |
| Current version         | 0.5.1.0             |
| OS                      | Windows             |
| Word size               | 32                  |
| Program type            | Portable and not    |
| License                 | GNU GPLv3           |