#include "QTimeAlarm.h"

QTimeAlarm::QTimeAlarm(QObject *parent)
    : QObject(parent)
    , _timer_event_period(333)
    , _default_clocks_type("hh:mm:ss")
    , _zero_time("00:00:00")
    , _path_to_sound("sounds/AlarmMusic.mp3")
{
    startTimer(_timer_event_period);
    connect(this, &QTimeAlarm::disableAlarm,
            this, &QTimeAlarm::disable_music);
    connect(this, &QTimeAlarm::disableTimer,
            this, &QTimeAlarm::disable_music);
    connect(this, &QTimeAlarm::disableTimer,
            this, &QTimeAlarm::disable_timer);
}

void QTimeAlarm::timerEvent(QTimerEvent *)
{
    if (_settings_menu->get_alarm_state())
    {
        if (QTime::currentTime().toString(_default_clocks_type) == _settings_menu->alarm_time_value().toString(_default_clocks_type))
        {
            enable_music();
            show_message(tr("Alarm"), tr("Current time is ", "Alarm: It is show current time") + QTime::currentTime().toString(_default_clocks_type));
            emit disableAlarm();
        }
    }
    if(_timer_is_enabled)
    {
        emit timerMinusTime(_timer_event_period);
        _timer = _timer.addMSecs(-_timer_event_period);
        if (_timer.toString(_default_clocks_type) == _zero_time)
        {
            enable_music();
            show_message(tr("Timer"), tr("Timer over"));
            emit disableTimer();
        }
    }
    set_clocks();
}

void QTimeAlarm::set_settings_menu(ClocksSettings *settings_menu)
{
    _settings_menu = settings_menu;
}

void QTimeAlarm::enable_timer(int h, int m, int s, int ms)
{
    _timer_is_enabled = true;
    _timer = QTime(h, m, s, ms);
}

void QTimeAlarm::disable_timer()
{
    _timer_is_enabled = false;
}

void QTimeAlarm::enable_music()
{
    _alarm_player = new QMediaPlayer;
    _alarm_music = new QMediaPlaylist;
    _alarm_music->addMedia(QUrl::fromLocalFile(_path_to_sound));
    _alarm_music->setPlaybackMode(QMediaPlaylist::Loop);
    _alarm_player->setPlaylist(_alarm_music);
    _alarm_player->setVolume(100);
    _alarm_player->play();
}

void QTimeAlarm::disable_music()
{
    _alarm_player->stop();
    delete _alarm_player;
    delete _alarm_music;
}

void QTimeAlarm::set_clocks()
{
    if(_settings_menu->get_timer_or_clocks() == Qt::CheckState())
    {
        emit setCurrentTime();
    }
    else
    {
        emit sendTime(&_timer);
    }
}

void QTimeAlarm::show_message(QString header, QString message)
{
    auto info = std::make_unique<QMessageBox>(QMessageBox::Information, header, message, QMessageBox::Ok);
    info->setStyleSheet(_settings_menu->get_css_style());
    info->exec();
}
