#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _settings_menu = new ClocksSettings;
    _label_clocks = std::make_unique<QLabelClocks>(this, _settings_menu);
    _time_alarm = new QTimeAlarm(this);
    _time_alarm->set_settings_menu(_settings_menu);

    QMenu *menu = new QMenu(this);
    QAction *open_settings_action = new QAction(tr("Open settings"));
    QAction *close_program_action = new QAction(tr("Close"));
    menu->addAction(open_settings_action);
    menu->addAction(close_program_action);

    _tray_icon = std::make_unique<QSystemTrayIcon>(this);
    QIcon icon(":images/icons/baseline_schedule_white_48dp.png");
    _tray_icon->setIcon(icon);
    _tray_icon->setToolTip(tr("Clocks On Windows"));
    _tray_icon->setContextMenu(menu);
    _tray_icon->show();

    connect(open_settings_action, &QAction::triggered,
            this, &MainWindow::show_settings_menu);
    connect(close_program_action, &QAction::triggered,
            this, &MainWindow::close_all);
    connect(_tray_icon.get(), &QSystemTrayIcon::activated,
            this, &MainWindow::icon_activated);

    connect(_settings_menu, &ClocksSettings::fontChanged,
            _label_clocks.get(), &QLabelClocks::font_changed);
    connect(_settings_menu, &ClocksSettings::fontSizeChanged,
            _label_clocks.get(), &QLabelClocks::set_font_size);
    connect(_settings_menu, &ClocksSettings::clocksTypeChanged,
            _label_clocks.get(), &QLabelClocks::set_clocks_type);
    connect(_settings_menu, &ClocksSettings::cssStyleChanged,
            _label_clocks.get(), &QLabelClocks::set_css_style);
    connect(_settings_menu, &ClocksSettings::cssStyleChanged,
            _label_clocks.get(), &QLabelClocks::update_style);
    connect(_settings_menu, &ClocksSettings::redChanged,
            _label_clocks.get(), &QLabelClocks::set_red);
    connect(_settings_menu, &ClocksSettings::greenChanged,
            _label_clocks.get(), &QLabelClocks::set_green);
    connect(_settings_menu, &ClocksSettings::blueChanged,
            _label_clocks.get(), &QLabelClocks::set_blue);
    connect(_settings_menu, &ClocksSettings::alphaChanged,
            _label_clocks.get(), &QLabelClocks::set_alpha);

    connect(_label_clocks.get(), &QLabelClocks::clocksWidthChanged,
            _settings_menu, &ClocksSettings::set_clocks_width);

    connect(_time_alarm, &QTimeAlarm::sendTime,
            _label_clocks.get(), &QLabelClocks::set_time);
    connect(_time_alarm, &QTimeAlarm::setCurrentTime,
            _label_clocks.get(), &QLabelClocks::set_current_time);
    connect(_time_alarm, &QTimeAlarm::disableAlarm,
            _settings_menu, &ClocksSettings::disable_alarm);
    connect(_time_alarm, &QTimeAlarm::disableTimer,
            _settings_menu, &ClocksSettings::disable_timer);
    connect(_time_alarm, &QTimeAlarm::timerMinusTime,
            _settings_menu, &ClocksSettings::timer_minus_time);
    connect(_settings_menu, &ClocksSettings::enableTimer,
            _time_alarm, &QTimeAlarm::enable_timer);
    connect(_settings_menu, &ClocksSettings::disableTimer,
            _time_alarm, &QTimeAlarm::disable_timer);
}

MainWindow::~MainWindow()
{
    delete _settings_menu;
    delete _time_alarm;
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        _last_point = event->pos();
        _is_moving = true;
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons() == Qt::LeftButton && _is_moving)
    {
        move(event->globalX() - _last_point.x(), event->globalY() - _last_point.y());
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && _is_moving)
    {
        _is_moving = false;
        _label_clocks->set_is_moving(_is_moving);
        _settings_menu->set_coordinate_x(event->globalX() - _last_point.x());
        _settings_menu->set_coordinate_y(event->globalY() - _last_point.y());
    }
}

void MainWindow::icon_activated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    {
        if(!_settings_menu->isVisible())
        {
            _settings_menu->show();
        }
        break;
    }
    default:
    {
        break;
    }
    }
}

void MainWindow::show_settings_menu()
{
    if(!_settings_menu->isVisible())
    {
        _settings_menu->show();
    }
}

void MainWindow::close_all()
{
    _settings_menu->close();
    close();
}

void MainWindow::showEvent(QShowEvent *event)
{
    // It is needed for show correct clocks after constructor of MainWindow
    // If update some MainWindow stuff before contructor compliting, we will see uncorrect clocks
    _label_clocks->init();
    _settings_menu->restore_settings();
}
