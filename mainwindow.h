#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include "QLabelClocks.h"
#include "QTimeAlarm.h"
#include "QMouseEvent"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    using QLabelClocksPtr = std::unique_ptr<QLabelClocks>;
    using QSystemTrayIconPtr = std::unique_ptr<QSystemTrayIcon>;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private slots:
    void icon_activated(QSystemTrayIcon::ActivationReason reason);
    void show_settings_menu();
    void close_all();

private:
    QLabelClocksPtr _label_clocks;
    QTimeAlarm *_time_alarm;
    ClocksSettings *_settings_menu;
    QSystemTrayIconPtr _tray_icon;
    bool _is_moving = false;
    QPoint _last_point;
    Ui::MainWindow *ui;

protected:
    void showEvent(QShowEvent *event) override;
};

#endif // MAINWINDOW_H
