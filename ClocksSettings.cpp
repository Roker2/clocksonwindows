#include "ClocksSettings.h"
#include "ui_ClocksSettings.h"

ClocksSettings::ClocksSettings(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ClocksSettings)
    , _save_settings(std::make_unique<QSaveSettings>())
    , _restore_settings(std::make_unique<QRestoreSettings>())
{
    ui->setupUi(this);
    ui->textEdit_CSS->hide();
    QFile CSSFile("themes/" + _theme_name+ "/" + _theme_name + ".css");
    if(CSSFile.open(QIODevice::ReadOnly))
    {
        _css_style = CSSFile.readAll();
        CSSFile.close();
        setStyleSheet(_css_style);
    }
    setFixedSize(height(), width());
}

ClocksSettings::~ClocksSettings()
{
    SaveSettings();
    delete ui;
}

void ClocksSettings::on_Button_SetWhite_clicked()
{
    ui->Slider_Red->setValue(255);
    ui->Slider_Green->setValue(255);
    ui->Slider_Blue->setValue(255);
}

void ClocksSettings::on_Button_SetGrey_clicked()
{
    ui->Slider_Red->setValue(122);
    ui->Slider_Green->setValue(122);
    ui->Slider_Blue->setValue(122);
}

void ClocksSettings::on_Button_SetBlack_clicked()
{
    ui->Slider_Red->setValue(0);
    ui->Slider_Green->setValue(0);
    ui->Slider_Blue->setValue(0);
}

int ClocksSettings::get_coordinate_x()
{
    return ui->Edit_coordinate_x->text().toInt();
}

int ClocksSettings::get_coordinate_y()
{
    return ui->Edit_coordinate_y->text().toInt();
}

int ClocksSettings::get_font_size()
{
    return ui->Slider_FontSize->value();
}
int ClocksSettings::get_font_size_max()
{
    return ui->Slider_FontSize->maximum();
}

void ClocksSettings::on_fontComboBox_currentFontChanged(const QFont &f)
{
    QApplication::setFont(f);
    emit fontChanged();
}

void ClocksSettings::on_Button_RightUp_clicked()
{
    ui->Edit_coordinate_x->setText(QString::number(QApplication::desktop()->width() - _clocks_width));
    ui->Edit_coordinate_y->setText("0");
}

void ClocksSettings::on_Button_RightDown_clicked()
{
    ui->Edit_coordinate_x->setText(QString::number(QApplication::desktop()->width() - _clocks_width));
    ui->Edit_coordinate_y->setText(QString::number(QApplication::desktop()->height() - get_font_size()));
}

void ClocksSettings::on_Button_LeftDown_clicked()
{
    ui->Edit_coordinate_x->setText("0");
    ui->Edit_coordinate_y->setText(QString::number(QApplication::desktop()->height() - get_font_size()));
}

void ClocksSettings::on_Button_LeftUp_clicked()
{
    ui->Edit_coordinate_x->setText("0");
    ui->Edit_coordinate_y->setText("0");
}

QTime ClocksSettings::alarm_time_value()
{
    return ui->timeEdit_Alarm->time();
}

void ClocksSettings::enable_alarm()
{
    ui->Button_Enable_Disable_Alarm->setText(tr("Disable Alarm"));
    _alarm_state = true;
}

void ClocksSettings::disable_alarm()
{
    ui->Button_Enable_Disable_Alarm->setText(tr("Enable Alarm"));
    _alarm_state = false;
}

void ClocksSettings::on_Button_Enable_Disable_Alarm_clicked()
{
    if(!_alarm_state)
    {
        enable_alarm();
    }
    else
    {
        disable_alarm();
    }
}

bool ClocksSettings::get_alarm_state()
{
    return _alarm_state;
}

QString ClocksSettings::get_type_clocks()
{
    return ui->Edit_ClocksType->text();
}

void ClocksSettings::on_Button_Enable_Disable_Timer_clicked()
{
    if(!_timer_state)
    {
        enable_timer();
    }
    else
    {
        disable_timer();
    }
}

bool ClocksSettings::get_timer_state()
{
    return _timer_state;
}

void ClocksSettings::enable_timer()
{
    ui->Button_Enable_Disable_Timer->setText(tr("Disable Timer"));
    _timer_state = true;
    ui->timeEdit_Timer->setReadOnly(true);
    ui->checkBox_clocks_or_timer->setCheckable(true);
    QTime timer = get_timer_value();
    emit enableTimer(timer.hour(), timer.minute(), timer.second(), timer.msec());
}

void ClocksSettings::disable_timer()
{
    ui->Button_Enable_Disable_Timer->setText(tr("Enable Timer"));
    _timer_state = false;
    ui->timeEdit_Timer->setReadOnly(false);
    ui->checkBox_clocks_or_timer->setCheckable(false);
    emit disableTimer();
}

void ClocksSettings::timer_minus_time(int milliseconds)
{
    ui->timeEdit_Timer->setTime(get_timer_value().addMSecs(-milliseconds));
}

void ClocksSettings::set_clocks_width(int clocks_width)
{
    _clocks_width = clocks_width;
}

QTime ClocksSettings::get_timer_value()
{
    return ui->timeEdit_Timer->time();
}

Qt::CheckState ClocksSettings::get_timer_or_clocks()
{
    return ui->checkBox_clocks_or_timer->checkState();
}

Qt::CheckState ClocksSettings::get_use_css()
{
    return ui->checkBox_UseCSS->checkState();
}

void ClocksSettings::on_Slider_Red_valueChanged(int value)
{
    _red = value;
    emit redChanged(_red);
}

void ClocksSettings::on_Slider_Green_valueChanged(int value)
{
    _green = value;
    emit greenChanged(_green);
}

void ClocksSettings::on_Slider_Blue_valueChanged(int value)
{
    _blue = value;
    emit blueChanged(_blue);
}


void ClocksSettings::on_Slider_Transparent_valueChanged(int value)
{
    _alpha = value;
    emit alphaChanged(_alpha);
}

QString ClocksSettings::get_css_style()
{
    return _css_style;
}

void ClocksSettings::restore_settings()
{
    _restore_settings->Restore();
    _restore_settings->RestoreCSS();
    RestoreSettings_End();
}

void ClocksSettings::on_Button_ImportSettings_clicked()
{
    _restore_settings->Restore(QFileDialog::getOpenFileName(this, tr("Import Settings"), "", "*.save"));
    RestoreSettings_End();
}

void ClocksSettings::RestoreSettings_End()
{
    ui->Slider_Red->setValue((*_restore_settings->ReturnRedValue()));
    ui->Slider_Green->setValue((*_restore_settings->ReturnGreenValue()));
    ui->Slider_Blue->setValue((*_restore_settings->ReturnBlueValue()));
    ui->Slider_Transparent->setValue((*_restore_settings->ReturnAlphaValue()));
    ui->Edit_coordinate_x->setText(QString::number(*_restore_settings->ReturnXValue()));
    ui->Edit_coordinate_y->setText(QString::number(*_restore_settings->ReturnYValue()));
    ui->Edit_ClocksType->setText((*_restore_settings->ReturnClocksTypeValue()));
    ui->Slider_FontSize->setValue((*_restore_settings->ReturnFontSizeValue()));
    ui->checkBox_UseCSS->setCheckState((Qt::CheckState)(*_restore_settings->ReturnUseCSS()));
    if((*_restore_settings->RestoreClocksCSSStyle()) != "")
        ui->textEdit_CSS->setText((*_restore_settings->RestoreClocksCSSStyle()));
    if((*_restore_settings->RestoreFontName()) != "")
        ui->fontComboBox->setCurrentFont(QFont((*_restore_settings->RestoreFontName())));
}

void ClocksSettings::SaveSettings_Start()
{
    _save_settings->SetRedValue(&_red);
    _save_settings->SetGreenValue(&_green);
    _save_settings->SetBlueValue(&_blue);
    _save_settings->SetAlphaValue(&_alpha);
    _save_settings->SetXValue(get_coordinate_x());
    _save_settings->SetYValue(get_coordinate_y());
    _save_settings->SetClocksTypeValue(get_type_clocks());
    _save_settings->SetFontSize(get_font_size());
    _save_settings->SetUseCSS(get_use_css());
    _save_settings->SetFontName(QFontInfo(ui->fontComboBox->currentFont()).family());
}

void ClocksSettings::SaveSettings()
{
    SaveSettings_Start();
    _save_settings->Save();
    _save_settings->SetClocksCSSStyle(get_clocks_css_style());
    _save_settings->SaveCSS();
}

void ClocksSettings::on_Button_ExportSettings_clicked()
{
    SaveSettings_Start();
    _save_settings->Save(QFileDialog::getSaveFileName(this, tr("Export Settings"), "", "*.save"));
}

void ClocksSettings::on_AboutProgram_OpenSources_clicked()
{
    QDesktopServices::openUrl(QUrl("https://gitlab.com/Roker2/clocksonwindows"));
}

void ClocksSettings::on_AboutProgram_Donation_clicked()
{
    QDesktopServices::openUrl(QUrl("https://qiwi.me/roker2"));
}

void ClocksSettings::on_checkBox_UseCSS_stateChanged(int arg1)
{
    if(arg1)
        ui->textEdit_CSS->show();
    else
        ui->textEdit_CSS->hide();
    emit cssStyleChanged(ui->textEdit_CSS->toPlainText());
}

QString ClocksSettings::get_clocks_css_style()
{
    return ui->textEdit_CSS->toPlainText();
}

void ClocksSettings::on_textEdit_CSS_textChanged()
{
    emit cssStyleChanged(ui->textEdit_CSS->toPlainText());
}

void ClocksSettings::set_coordinate_x(int value)
{
    ui->Edit_coordinate_x->setText(QString::number(value));
}

void ClocksSettings::set_coordinate_y(int value)
{
    ui->Edit_coordinate_y->setText(QString::number(value));
}

void ClocksSettings::on_Slider_FontSize_valueChanged(int value)
{
    emit fontSizeChanged(value);
}

void ClocksSettings::on_Edit_ClocksType_textChanged(const QString &arg1)
{
    emit clocksTypeChanged(arg1);
}
